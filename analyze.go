package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const flagBuild = "build"

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolTFlag{
			Name:  flagBuild,
			Usage: "Build Java application. It's not needed if the code is already compiled.",
		},
	}
}

const (
	pathGradle           = "/usr/bin/gradle"
	pathExtraBuildGradle = "/fsb/build.gradle"
	pathOutput           = "/tmp/findSecurityBugs.xml"
)

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	// Append extra configuration to build.gradle
	from, err := os.Open(pathExtraBuildGradle)
	if err != nil {
		return nil, err
	}
	defer from.Close()

	target := filepath.Join(path, "build.gradle")
	to, err := os.OpenFile(target, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}
	defer to.Close()

	if _, err := io.Copy(to, from); err != nil {
		return nil, err
	}

	// Build Java source code if needed
	if c.BoolT(flagBuild) {
		// Ignore the exit status since the build may fail
		// because of the test suite.
		setupCmd(exec.Command(pathGradle, "build")).Run()
	}

	// Run findSecBugs task
	if err := setupCmd(exec.Command(pathGradle, "findSecBugs")).Run(); err != nil {
		return nil, err
	}

	return os.Open(pathOutput)
}
