# find-sec-bugs Gradle analyzer

**As of GitLab %11.9, this repository is deprecated, and replaced by the new [spotbugs](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs/) project.**

GitLab Analyzer for Gradle projects. It's based on [find-sec-bugs](https://find-sec-bugs.github.io/).

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
documents how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
